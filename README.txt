Programmer: Dallas Wales
Date Last Modified: May 03 2015

Build Instructions: javac src/interview/*.java

Run Instructions: java -cp "src" interview.TopResults
Make sure you are in the NTENT/ directory before issuing the above command.

Summary: This program reads a txt file within the NTENT project folder and
returns the top 100 words used in the file by placing them into a Map, and then
sorting them by placing them through a linked list. It finally iterates through
the linked list, printing out the rank, word, count, and frequency 
respectively.

Known issues: In Eclipse, the file is successfully read into UTF-8 
(I changed the BufferedReader to make the java file do the work there).
However, via my cmd.exe terminal some text is still returned as gibberish.