package interview;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class TopResults {

	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		// The file to be scanned, relative path.
		String fileName = "InputText.txt";

		Map<String, Integer> myMap = new HashMap<String, Integer>();
		int totalWords = fillMap(fileName, myMap);
		printTopResults(sortMap(myMap), 100, totalWords);
	}

	/*
	 * This method scans a .txt file and takes account of the times that each
	 * word is used in the text.
	 */
	private static int fillMap(String file, Map<String, Integer> map)
			throws IOException, FileNotFoundException {
		String line;
		int count = 0;
		BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(file), "UTF-8"));
		try {
			while ((line = br.readLine()) != null) {
				String lowerCased = line.toLowerCase();
				String[] temp = lowerCased
						.split("[\\p{Blank}\\\\\\p{Punct}&&[^-]]");
				count = count + temp.length;
				for (int i = 0; i < temp.length; i++) {
					map.compute(temp[i], (k, v) -> v == null ? 1 : v + 1);
				}
			}
		} catch (Exception e) {
			br.close();
			e.printStackTrace();
		} finally {
			map.remove("");
			br.close();
		}
		return count;
	}

	// Takes an unsorted map and converts it into a sorted linked list.
	private static List<Entry<String, Integer>> sortMap(
			Map<String, Integer> theMap) {

		List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(
				theMap.entrySet());

		Collections.sort(list, new Comparator<Entry<String, Integer>>() {

			@Override
			// Taken from Java Collections: Sorting Methods
			public int compare(Entry<String, Integer> o1,
					Entry<String, Integer> o2) {
				return o2.getValue().compareTo(o1.getValue());
			}
		});

		return list;
	}

	// Takes in a sorted list created from sortMap() and prints the top results
	public static void printTopResults(List<Entry<String, Integer>> theList,
			int topNumbers, double wordTotal)
			throws UnsupportedEncodingException {
		PrintStream out = new PrintStream(System.out, true, "UTF-8");
		int counter = 0, rank = 1;
		out.println("Rank: \t Word: \t Count: \t Frequency:");
		for (Entry<String, Integer> entry : theList) {
			if (counter >= topNumbers)
				break;
			out.printf("%s\t %s\t %s\t %.6s%n", rank, entry.getKey(),
					entry.getValue(), (entry.getValue() / wordTotal));
			counter++;
			rank++;
		}
	}

}
